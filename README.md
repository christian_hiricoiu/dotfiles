# dotfiles

## Configuration

- OS : Arch-Linux
- Desktop : KDE Plasma
- terminal : alacritty
- shell : zsh + oh-my-zsh custom darkblood based theme + plugins
- code editor : neovim
- main font : [`ttf-jetsbrains-mono-nerd`](https://www.jetbrains.com/fr-fr/lp/mono/)

## Deploy
Just copy the content of `home` folder inside `$HOME` :
```shell
cp -r home/* $HOME
```
