--[[ signification:
while in normal mode "n",
leader=whitespace+"pv" is equivalent of cmd "Ex"
which is close file and return to file Explorer
]]--
vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)
