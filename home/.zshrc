export ZSH="$HOME/.oh-my-zsh"

ZSH_THEME="darkblood"

HIST_STAMPS="dd/mm/yyyy"

plugins=(git)

source $ZSH/oh-my-zsh.sh

export LANG=en_US.UTF-8

export EDITOR='vim'

alias ll="ls -la"